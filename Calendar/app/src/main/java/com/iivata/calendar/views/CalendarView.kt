package com.iivata.calendar.views

import android.content.Context
import android.util.AttributeSet
import android.view.LayoutInflater
import android.widget.AdapterView
import android.widget.LinearLayout
import com.iivata.calendar.adapters.CalendarAdapter
import com.iivata.calendar.R
import kotlinx.android.synthetic.main.month_calendar.view.*
import org.jetbrains.anko.onClick
import org.jetbrains.anko.onItemClick
import org.jetbrains.anko.toast
import java.text.SimpleDateFormat
import java.util.*

class CalendarView : LinearLayout {

    private val DATE_FORMAT : String = "MMM yyyy"
    private val CALENDAR_LENGTH : Int = 42
    private var mContext : Context? = null
    private var mCurrentCalendar : Calendar = Calendar.getInstance()
    private var mOnItemClickListener: AdapterView.OnItemClickListener? = null;

    constructor(context: Context) : super(context) {
        mContext = context
        inflate()
        setupClicks()
    }

    constructor(context: Context, attrs : AttributeSet) : super(context, attrs) {
        mContext = context
        inflate()
        setupClicks()
    }

    constructor(context: Context, attrs : AttributeSet, defStyleAttr : Int) : super(context, attrs, defStyleAttr) {
        mContext = context
        inflate()
        setupClicks()
    }

    private fun inflate() {
        var inflater : LayoutInflater = LayoutInflater.from(mContext)
        inflater.inflate(R.layout.month_calendar, this)
        updateCalendar()
    }

    // public by default
    fun updateCalendar() {
        var cells : ArrayList<Date> = ArrayList()
//        var calendar : Calendar = Calendar.getInstance()
        var calendar : Calendar = mCurrentCalendar.clone() as Calendar
        calendar.set(Calendar.DAY_OF_MONTH, 1)
        var monthBeginningCell : Int = calendar.get(Calendar.DAY_OF_WEEK) - 2
        calendar.add(Calendar.DAY_OF_MONTH, - monthBeginningCell)
        while (cells.size < CALENDAR_LENGTH) {
            cells.add(calendar.time)
            calendar.add(Calendar.DAY_OF_MONTH, 1)
        }
        this.grid_view.adapter = CalendarAdapter(context, cells)

        val sdf : SimpleDateFormat = SimpleDateFormat(DATE_FORMAT)
        month_text_view?.text = sdf.format(mCurrentCalendar.time)


    }

    private fun setupClicks() {
        this.grid_view?.onItemClickListener = mOnItemClickListener

        this.next_month_button?.onClick {
            mCurrentCalendar.add(Calendar.MONTH, 1)
            updateCalendar()
        }

        this.prev_month_button?.onClick {
            mCurrentCalendar.add(Calendar.MONTH, -1)
            updateCalendar()
        }
    }

    fun setOnDateClickListener(onItemClickListener: AdapterView.OnItemClickListener) {
        mOnItemClickListener = onItemClickListener
        setupClicks()
    }


}
