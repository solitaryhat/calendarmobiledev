package com.iivata.calendar.activities

import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.support.v4.view.ViewPager
import com.iivata.calendar.R
import com.iivata.calendar.adapters.ViewPagerAdapter
import com.iivata.calendar.fragments.MonthFragment
import kotlinx.android.synthetic.main.activity_main.*

class MainActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        setSupportActionBar(toolbar)
        setupViewPager(view_pager)
        tabs.setupWithViewPager(view_pager)
    }

    private fun setupViewPager(viewPager: ViewPager) {
        val adapter = ViewPagerAdapter(supportFragmentManager)
        adapter.addFragment(MonthFragment(), getString(R.string.month_tab))
        adapter.addFragment(MonthFragment(), getString(R.string.today_tab))
        viewPager.adapter = adapter
    }
}
