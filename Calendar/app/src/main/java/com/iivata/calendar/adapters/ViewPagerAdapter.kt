package com.iivata.calendar.adapters

import android.support.v4.app.Fragment
import android.support.v4.app.FragmentManager
import android.support.v4.app.FragmentPagerAdapter
import java.util.*

class ViewPagerAdapter : FragmentPagerAdapter {

    val mFragmentList: ArrayList<Fragment> = arrayListOf()
    val mFragmentTitleList: ArrayList<String> = arrayListOf()

    constructor(fragmentManager: FragmentManager) : super(fragmentManager)

    override fun getItem(p0: Int): Fragment? {
        return mFragmentList[p0]
    }

    override fun getCount(): Int {
        return mFragmentList.size
    }

    fun addFragment(fragment: Fragment, title: String) {
        mFragmentList.add(fragment)
        mFragmentTitleList.add(title)
    }

    override fun getPageTitle(position: Int): CharSequence? {
        return mFragmentTitleList[position]
    }
}