package com.iivata.calendar.fragments

import android.os.Bundle
import android.support.v4.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.AdapterView
import android.widget.CalendarView
import com.iivata.calendar.R
import kotlinx.android.synthetic.main.fragment_month.*
import kotlinx.android.synthetic.main.fragment_month.view.*
import org.jetbrains.anko.find
import org.jetbrains.anko.onClick
import org.jetbrains.anko.support.v4.toast
import java.util.*

class MonthFragment : Fragment() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
    }

    override fun onCreateView(inflater: LayoutInflater?, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        // Inflate the layout for this fragment
        var view : View = inflater!!.inflate(R.layout.fragment_month, container, false)
//        var calendar : CalendarView? = view.find<CalendarView>(R.id.calendarView)
//        calendar?.setOnClickListener(AdapterView.OnItemClickListener { adapterView, view, i, l ->
//            val date = adapterView?.getItemAtPosition(i) as Date
//            toast(date.toString())
//        })
        return view;
    }

}// Required empty public constructor
