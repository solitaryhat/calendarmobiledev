package com.iivata.calendar.adapters

import android.content.Context
import android.graphics.Color
import android.graphics.Typeface
import android.text.format.DateUtils
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ArrayAdapter
import android.widget.TextView
import com.iivata.calendar.R
import org.jetbrains.anko.textColor
import java.util.*

class CalendarAdapter : ArrayAdapter<Date> {

    var inflater : LayoutInflater? = null


    constructor(context: Context, days : ArrayList<Date>) : super(context, R.layout.month_calendar, days) {
        inflater = LayoutInflater.from(context)
    }

    override fun getView(pos: Int, v: View?, parent: ViewGroup?): View? {
        var date : Date = getItem(pos)

        var today : Date = Date()

        // inflate not existent items
        var view : View? = v
        if (view == null) {
            view = inflater?.inflate(R.layout.month_calendar_item, parent, false)
        }

        var dayTextView : TextView? = view?.findViewById(R.id.day_text_view) as TextView


        dayTextView?.setTypeface(null, Typeface.NORMAL)
        if (date.month != today.month || date.year != today.year) {
            dayTextView?.textColor = Color.GRAY
        } else if (isToday(date)) {
            dayTextView?.textColor = Color.BLUE
            dayTextView?.setTypeface(null, Typeface.BOLD)
        } else {
            dayTextView?.textColor = Color.BLACK
        }

        dayTextView?.text = date.date.toString()

        var dayActivitiesCount : TextView? = view?.findViewById(R.id.tasks_count_text_view) as TextView
        //dayActivitiesCount?.textScaleX = 0.5f
//        dayActivitiesCount?.textSize = dayTextView!!.textSize / 4
//        dayActivitiesCount?.gravity = (Gravity.BOTTOM or Gravity.END)
        dayActivitiesCount?.visibility = View.GONE

        return view;
    }

    private fun isToday(date : Date) : Boolean = DateUtils.isToday(date.time)
}
